package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import java.io.IOException;
import java.util.Iterator;

import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.XMLParser;

public class Application {
	public static void main(String[] args)
			throws InvalidXMLException, IOException, ResourceInitializationException, AnalysisEngineProcessException {
		String text = "Satya Nadella is the ceo of Microsoft";

		XMLParser parser = UIMAFramework.getXMLParser();
		AnalysisEngineDescription aeDesc = parser.parseAnalysisEngineDescription(
				new XMLInputSource(Application.class.getResource("CeoOfAnnotator.xml")));

		AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(aeDesc);
		try {
			JCas aJCas = ae.newJCas();

			aJCas.setDocumentText(text);
			aJCas.setDocumentLanguage("en");

			ae.process(aJCas);

			// aJCas.reset();

			System.out.println("--- All annotations (begin) ---");
			Iterator<Annotation> it = aJCas.getAnnotationIndex().iterator();
			while (it.hasNext()) {
				Annotation nextAnnot = it.next();
				System.out.print(nextAnnot);
				System.out.println(String.format("covered text: \"%s\"", nextAnnot.getCoveredText()));
				System.out.println();
			}
			System.out.println("--- All annotations (end)");

			System.out.println();

			System.out.println("--- Company Annotations (begin) ---");
			Iterator<Company> it2 = aJCas.getAnnotationIndex(Company.class).iterator();
			while (it2.hasNext()) {
				Company nextCompany = it2.next();
				System.out.print(nextCompany);
				System.out.println(String.format("covered text: \"%s\"", nextCompany.getCoveredText()));
				System.out.println();
			}
			System.out.println("--- Company Annotations (begin) ---");

		} finally {
			ae.destroy();
		}
	}
}
