package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import java.util.Collection;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

public class ApplicationFIT {
	public static void main(String[] args) throws UIMAException {
		AnalysisEngineDescription ceoOfAnnotDescr = AnalysisEngineFactory.createEngineDescription(
				CeoOfAnnotatorFIT.class, CeoOfAnnotatorFIT.PARAM_PATTERN,
				"(?<ceo>" + CeoOfAnnotatorFIT.NAME_PATTERN_STRING
						+ ")\\s+is\\s+the\\s+(?:ceo|chief\\s+executive\\s+officer)\\s+of\\s+(?<company>"
						+ CeoOfAnnotatorFIT.NAME_PATTERN_STRING + ")");

		JCas aJCas = JCasFactory.createJCas();

		aJCas.setDocumentText("Satya Nadella is the chief executive officer of Microsoft");
		aJCas.setDocumentLanguage("en");

		SimplePipeline.runPipeline(aJCas, ceoOfAnnotDescr);

		Collection<CeoOf> ceos = JCasUtil.select(aJCas, CeoOf.class);
		System.out.println(ceos);
	}
}
