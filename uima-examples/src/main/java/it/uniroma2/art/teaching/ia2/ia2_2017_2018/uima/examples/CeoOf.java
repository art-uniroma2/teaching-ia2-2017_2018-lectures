

/* First created by JCasGen Fri Nov 17 12:22:26 CET 2017 */
package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Nov 20 14:18:51 CET 2017
 * XML source: C:/Users/Manuel/WORK/ART/Workspace/IA2-2017_2018/Eclipse/uima-examples/src/main/java/it/uniroma2/art/teaching/ia2/ia2_2017_2018/uima/examples/TypeSystem.xml
 * @generated */
public class CeoOf extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(CeoOf.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected CeoOf() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public CeoOf(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public CeoOf(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public CeoOf(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: ceo

  /** getter for ceo - gets 
   * @generated
   * @return value of the feature 
   */
  public Person getCeo() {
    if (CeoOf_Type.featOkTst && ((CeoOf_Type)jcasType).casFeat_ceo == null)
      jcasType.jcas.throwFeatMissing("ceo", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    return (Person)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((CeoOf_Type)jcasType).casFeatCode_ceo)));}
    
  /** setter for ceo - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setCeo(Person v) {
    if (CeoOf_Type.featOkTst && ((CeoOf_Type)jcasType).casFeat_ceo == null)
      jcasType.jcas.throwFeatMissing("ceo", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    jcasType.ll_cas.ll_setRefValue(addr, ((CeoOf_Type)jcasType).casFeatCode_ceo, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: company

  /** getter for company - gets 
   * @generated
   * @return value of the feature 
   */
  public Company getCompany() {
    if (CeoOf_Type.featOkTst && ((CeoOf_Type)jcasType).casFeat_company == null)
      jcasType.jcas.throwFeatMissing("company", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    return (Company)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((CeoOf_Type)jcasType).casFeatCode_company)));}
    
  /** setter for company - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setCompany(Company v) {
    if (CeoOf_Type.featOkTst && ((CeoOf_Type)jcasType).casFeat_company == null)
      jcasType.jcas.throwFeatMissing("company", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    jcasType.ll_cas.ll_setRefValue(addr, ((CeoOf_Type)jcasType).casFeatCode_company, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    