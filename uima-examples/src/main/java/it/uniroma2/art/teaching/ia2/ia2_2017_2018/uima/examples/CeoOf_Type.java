
/* First created by JCasGen Fri Nov 17 12:22:26 CET 2017 */
package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Nov 20 14:18:51 CET 2017
 * @generated */
public class CeoOf_Type extends Annotation_Type {
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = CeoOf.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
 
  /** @generated */
  final Feature casFeat_ceo;
  /** @generated */
  final int     casFeatCode_ceo;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getCeo(int addr) {
        if (featOkTst && casFeat_ceo == null)
      jcas.throwFeatMissing("ceo", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    return ll_cas.ll_getRefValue(addr, casFeatCode_ceo);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setCeo(int addr, int v) {
        if (featOkTst && casFeat_ceo == null)
      jcas.throwFeatMissing("ceo", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    ll_cas.ll_setRefValue(addr, casFeatCode_ceo, v);}
    
  
 
  /** @generated */
  final Feature casFeat_company;
  /** @generated */
  final int     casFeatCode_company;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getCompany(int addr) {
        if (featOkTst && casFeat_company == null)
      jcas.throwFeatMissing("company", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    return ll_cas.ll_getRefValue(addr, casFeatCode_company);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setCompany(int addr, int v) {
        if (featOkTst && casFeat_company == null)
      jcas.throwFeatMissing("company", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf");
    ll_cas.ll_setRefValue(addr, casFeatCode_company, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public CeoOf_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_ceo = jcas.getRequiredFeatureDE(casType, "ceo", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.Person", featOkTst);
    casFeatCode_ceo  = (null == casFeat_ceo) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_ceo).getCode();

 
    casFeat_company = jcas.getRequiredFeatureDE(casType, "company", "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.Company", featOkTst);
    casFeatCode_company  = (null == casFeat_company) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_company).getCode();

  }
}



    