package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CASRuntimeException;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.CasIOUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.util.TypeSystemUtil;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Lemma;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordLemmatizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordParser;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;

public class ApplicationDKPRO {
	public static void main(String[] args) throws UIMAException, IOException, CASRuntimeException, SAXException {

		List<AnalysisEngineDescription> aes = new ArrayList<>();

		AnalysisEngineDescription segDescr = AnalysisEngineFactory.createEngineDescription(StanfordSegmenter.class);
		aes.add(segDescr);

		AnalysisEngineDescription posDescr = AnalysisEngineFactory.createEngineDescription(StanfordPosTagger.class);
		aes.add(posDescr);
		AnalysisEngineDescription lemDescr = AnalysisEngineFactory.createEngineDescription(StanfordLemmatizer.class);
		aes.add(lemDescr);

		AnalysisEngineDescription nerDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordNamedEntityRecognizer.class);
		aes.add(nerDescr);

		AnalysisEngineDescription parserDescr = AnalysisEngineFactory.createEngineDescription(StanfordParser.class);
		aes.add(parserDescr);

		JCas jCas = JCasFactory.createJCas();
		jCas.setDocumentLanguage("en");
		jCas.setDocumentText("Mary is nice. Paul is not. Furthermore, he is is not intelligent.");

		SimplePipeline.runPipeline(jCas, aes.toArray(new AnalysisEngineDescription[aes.size()]));

		File outputXmi = new File(ApplicationDKPRO.class.getSimpleName() + ".xmi");
		File outputXCas = new File(ApplicationDKPRO.class.getSimpleName() + ".xml");
		CasIOUtil.writeXmi(jCas.getCas(), outputXmi);
		CasIOUtil.writeXCas(jCas.getCas(), outputXCas);

		File outputTS = new File(ApplicationDKPRO.class.getSimpleName() + "-TypeSystem.xml");
		TypeSystemUtil.typeSystem2TypeSystemDescription(jCas.getTypeSystem()).toXML(new FileWriter(outputTS));
	
		Collection<Token> tokens = JCasUtil.select(jCas, Token.class);
		System.out.println(tokens);
		
		System.out.println("---");
		Collection<Lemma> lemmas = JCasUtil.select(jCas, Lemma.class);
		System.out.println(lemmas);

	}
}
