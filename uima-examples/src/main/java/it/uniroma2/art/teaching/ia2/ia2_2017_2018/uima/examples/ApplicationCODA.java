package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.XMLParser;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.standalone.CODAStandaloneFactory;
import it.uniroma2.art.coda.structures.ARTTriple;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;

public class ApplicationCODA {
	public static void main(String[] args) throws Exception {
		String text = "Satya Nadella is the ceo of Microsoft";

		XMLParser parser = UIMAFramework.getXMLParser();
		AnalysisEngineDescription aeDesc = parser.parseAnalysisEngineDescription(
				new XMLInputSource(ApplicationCODA.class.getResource("CeoOfAnnotator.xml")));

		AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(aeDesc);
		try {
			JCas aJCas = ae.newJCas();

			aJCas.setDocumentText(text);
			aJCas.setDocumentLanguage("en");

			ae.process(aJCas);

			// aJCas.reset();

			System.out.println("--- All annotations (begin) ---");
			Iterator<Annotation> it = aJCas.getAnnotationIndex().iterator();
			while (it.hasNext()) {
				Annotation nextAnnot = it.next();
				System.out.print(nextAnnot);
				System.out.println(String.format("covered text: \"%s\"", nextAnnot.getCoveredText()));
				System.out.println();
			}
			System.out.println("--- All annotations (end)");

			System.out.println();

			System.out.println("--- Company Annotations (begin) ---");
			Iterator<Company> it2 = aJCas.getAnnotationIndex(Company.class).iterator();
			while (it2.hasNext()) {
				Company nextCompany = it2.next();
				System.out.print(nextCompany);
				System.out.println(String.format("covered text: \"%s\"", nextCompany.getCoveredText()));
				System.out.println();
			}
			System.out.println("--- Company Annotations (begin) ---");

			SailRepository repository = new SailRepository(new MemoryStore());
			repository.initialize();
			try (RepositoryConnection conn = repository.getConnection()) {
				conn.setNamespace("", "http://example.org/"); // set default namespace
				File bundles = new File("bundles");
				if (!bundles.exists() || !bundles.isDirectory() || bundles.list().length == 0) {
					throw new RuntimeException(
							"Please create a directory named 'bundles' in the root of the project, and place the jar 'coda-converters-1.5-SNAPSHOT.jar' inside it");
				}

				File felixCache = new File("felixCache");
				if (!felixCache.exists() || !felixCache.isDirectory()) {
					throw new RuntimeException(
							"Please create a directory named 'felixCache' in the root of the project");
				}
				CODACore codaCore = CODAStandaloneFactory.getInstance(bundles, felixCache);
				codaCore.initialize(conn);
				try {
					codaCore.setJCas(aJCas);
					codaCore.setProjectionRulesModel(Application.class.getResourceAsStream("rules2.pr"));

					List<SuggOntologyCoda> processAllAnnotation = codaCore.processAllAnnotation(true);

					for (SuggOntologyCoda suggOntologyCoda : processAllAnnotation) {
						for (ARTTriple artTriple : suggOntologyCoda.getAllInsertARTTriple()) {
							conn.add(artTriple.getSubject(), artTriple.getPredicate(), artTriple.getObject());
						}
					}

					conn.export(Rio.createWriter(RDFFormat.NTRIPLES, System.out));
				} finally {
					codaCore.setRepositoryConnection(null); // avoid that the connection is closed
					codaCore.stopAndClose();
				}
			} finally {
				repository.shutDown();
			}
		} finally {
			ae.destroy();
		}
	}
}
