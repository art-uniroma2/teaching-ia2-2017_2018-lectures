package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;

// A naive annotator. Look here for more information on relation extraction: https://web.stanford.edu/class/cs124/lec/rel.pdf 
public class CeoOfAnnotator extends JCasAnnotator_ImplBase {

	private static final String NAME_PATTERN_STRING = "([A-Z][a-z]*(?:\\s+[A-Z][a-z]*)*)";

	private static Pattern pattern = Pattern
			.compile(NAME_PATTERN_STRING + "\\s+is\\s+the\\s+ceo\\s+of\\s+" + NAME_PATTERN_STRING);

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		String text = aJCas.getDocumentText();

		Matcher m = pattern.matcher(text);

		while (m.find()) {
			// obtains the span of the match associated with the first capture group (the
			// ceo)
			int ceoStart = m.start(1);
			int ceoEnd = m.end(1);
			Person person = new Person(aJCas, ceoStart, ceoEnd);
			person.addToIndexes(); // adds the annotation to the indexes

			// obtains the span of the match associated with the second capture group (the
			// company)
			int companyStart = m.start(2);
			int companyEnd = m.end(2);
			Company company = new Company(aJCas, companyStart, companyEnd);
			company.addToIndexes(); // adds the annotation to the indexes

			// obtains the span of the match associated with the entire regular expression
			int relationStart = m.start();
			int relationEnd = m.end();
			CeoOf relation = new CeoOf(aJCas, relationStart, relationEnd);
			relation.setCeo(person);
			relation.setCompany(company);

			// always edit an annotation before adding it to the indexes

			relation.addToIndexes(); // adds the annotation to the indexes
		}
	}

}
