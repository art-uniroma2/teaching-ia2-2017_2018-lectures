package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;

// A naive annotator. Look here for more information on relation extraction: https://web.stanford.edu/class/cs124/lec/rel.pdf 
@TypeCapability(outputs = { "it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.Person",
		"it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.Company",
		"it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples.CeoOf" })
public class CeoOfAnnotatorFIT extends JCasAnnotator_ImplBase {

	public static final String NAME_PATTERN_STRING = "(?:[A-Z][a-z]*(?:\\s+[A-Z][a-z]*)*)";

	private static final String DEFAULT_PATTERN = "(?<ceo>" + NAME_PATTERN_STRING
			+ ")\\s+is\\s+the\\s+ceo\\s+of\\s+(?<company>" + NAME_PATTERN_STRING + ")";

	public static final String PARAM_PATTERN = "pattern";

	@ConfigurationParameter(name = PARAM_PATTERN, defaultValue = DEFAULT_PATTERN)
	private Pattern pattern;

	public static final String CEO_ROLE = "ceo";
	public static final String COMPANY_ROLE = "company";
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		String text = aJCas.getDocumentText();

		Matcher m = pattern.matcher(text);

		while (m.find()) {
			// obtains the span of the match associated with the first capture group (the
			// ceo)
			int ceoStart = m.start(CEO_ROLE);
			int ceoEnd = m.end(CEO_ROLE);
			Person person = new Person(aJCas, ceoStart, ceoEnd);
			person.addToIndexes(); // adds the annotation to the indexes

			// obtains the span of the match associated with the second capture group (the
			// company)
			int companyStart = m.start(COMPANY_ROLE);
			int companyEnd = m.end(COMPANY_ROLE);
			Company company = new Company(aJCas, companyStart, companyEnd);
			company.addToIndexes(); // adds the annotation to the indexes

			// obtains the span of the match associated with the entire regular expression
			int relationStart = m.start();
			int relationEnd = m.end();
			CeoOf relation = new CeoOf(aJCas, relationStart, relationEnd);
			relation.setCeo(person);
			relation.setCompany(company);

			// always edit an annotation before adding it to the indexes

			relation.addToIndexes(); // adds the annotation to the indexes
		}
	}

}
