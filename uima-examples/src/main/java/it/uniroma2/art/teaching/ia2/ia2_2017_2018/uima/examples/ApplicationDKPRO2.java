package it.uniroma2.art.teaching.ia2.ia2_2017_2018.uima.examples;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CASRuntimeException;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.tools.cvd.CVD;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordLemmatizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordParser;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;

/**
 * This example is similar to {@link ApplicationDKPRO}, while it introduce the
 * use of CAS Visual Debugger (CVD) as a means for executing the pipeline over
 * arbitrary text and for browsing the annotations.
 */
public class ApplicationDKPRO2 {
	public static void main(String[] args) throws UIMAException, IOException, CASRuntimeException, SAXException {

		List<AnalysisEngineDescription> aes = new ArrayList<>();

		AnalysisEngineDescription segDescr = AnalysisEngineFactory.createEngineDescription(StanfordSegmenter.class);
		aes.add(segDescr);

		AnalysisEngineDescription posDescr = AnalysisEngineFactory.createEngineDescription(StanfordPosTagger.class);
		aes.add(posDescr);
		AnalysisEngineDescription lemDescr = AnalysisEngineFactory.createEngineDescription(StanfordLemmatizer.class);
		aes.add(lemDescr);

		AnalysisEngineDescription nerDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordNamedEntityRecognizer.class);
		aes.add(nerDescr);

		AnalysisEngineDescription parserDescr = AnalysisEngineFactory.createEngineDescription(StanfordParser.class);
		aes.add(parserDescr);

		AnalysisEngineDescription aaeDescr = AnalysisEngineFactory
				.createEngineDescription(aes.toArray(new AnalysisEngineDescription[aes.size()]));
		File aaeDescrFile = new File(ApplicationDKPRO2.class.getSimpleName() + "-aae.xml");
		aaeDescr.toXML(new FileWriter(aaeDescrFile));
		CVD.main(new String[] { "-desc", aaeDescrFile.getName() });
	}
}
