package it.uniroma2.art.teaching.ia2.ia2_2017_2018.rdf4j.examples;

import java.io.IOException;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class Example3 {
	public static void main(String[] args) throws RDFParseException, RepositoryException, IOException {
		MemoryStore memoryStore = new MemoryStore();
		Repository rep = new SailRepository(memoryStore);
		rep.initialize();
		try {
			try (RepositoryConnection conn = rep.getConnection()) {
				conn.add(Example3.class.getResource("example3.trig"), null, null);

				ValueFactory fact = conn.getValueFactory();
				
				try (RepositoryResult<Statement> statementResult = conn.getStatements(fact.createIRI("http://example.org#socrates"), null, null)) {
					while (statementResult.hasNext()) {
						Statement stmt = statementResult.next();
						System.out.println(stmt);
					}
				}
				
				System.out.println("----");

				Model aristotleModel = QueryResults.asModel(conn.getStatements(fact.createIRI("http://example.org#aristotle"), null, null));
				Rio.write(aristotleModel, System.out, RDFFormat.NQUADS);	
				
				System.out.println("----");

				Model aristotleModel2 = QueryResults.asModel(conn.getStatements(fact.createIRI("http://example.org#aristotle"), null, null, (Resource)null));
				Rio.write(aristotleModel2, System.out, RDFFormat.NQUADS);		
				
				System.out.println("----");

				conn.remove((Resource)null, null, null);

				conn.export(Rio.createWriter(RDFFormat.NQUADS, System.out));
				
				System.out.println("****");
			}
		} finally {
			rep.shutDown();
		}
	}
}
