package it.uniroma2.art.teaching.ia2.ia2_2017_2018.rdf4j.examples;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.FOAF;

public class Example1 {
	public static void main(String[] args) {
		ValueFactory fact = SimpleValueFactory.getInstance();

		IRI aIRI = fact.createIRI("http://example.org#socrates");
		Literal aLiteral = fact.createLiteral("socrates");
		BNode aBNode = fact.createBNode();
		
		System.out.println(aIRI);
		System.out.println(aLiteral);
		System.out.println(aBNode);
		
		Statement aStatement = fact.createStatement(aIRI, FOAF.NAME, aLiteral);
		
		System.out.println(aStatement);
		
		IRI ctx = fact.createIRI("http://example.org");
		
		Statement anotherStatement = fact.createStatement(aIRI, FOAF.NAME, aLiteral, ctx);
		
		System.out.println(anotherStatement);
	}
	
}
