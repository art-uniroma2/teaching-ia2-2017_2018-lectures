package it.uniroma2.art.teaching.ia2.ia2_2017_2018.rdf4j.examples;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;

public class Example2 {
	public static void main(String[] args) {
		ValueFactory fact = SimpleValueFactory.getInstance();

		IRI socratesIRI = fact.createIRI("http://example.org#socrates");
		Literal socratesName = fact.createLiteral("socrates");
		
		IRI aristotleIRI = fact.createIRI("http://example.org#aristotle");
		Literal aristotleName = fact.createLiteral("aristotle");

		IRI ctx = fact.createIRI("http://example.org");
		
		Model aModel = new LinkedHashModel();
		@SuppressWarnings("unused")
		Set<Statement> aSet = aModel;
		
		aModel.add(socratesIRI, RDF.TYPE, FOAF.PERSON);
		aModel.add(socratesIRI, FOAF.NAME, socratesName);
		
		aModel.add(aristotleIRI, RDF.TYPE, FOAF.PERSON, ctx);
		aModel.add(aristotleIRI, FOAF.NAME, aristotleName, ctx);

		RDFWriter turtleWriter = Rio.createWriter(RDFFormat.TURTLE, System.out);
		turtleWriter.set(BasicWriterSettings.PRETTY_PRINT, true);
		
		Rio.write(aModel, turtleWriter);
		
		System.out.println("----");
		
		Rio.write(aModel, System.out, RDFFormat.TRIG);
		
		System.out.println("----");

		aModel.setNamespace("", "http://example.org#");
		aModel.setNamespace(FOAF.NS);
		Rio.write(aModel, System.out, RDFFormat.TRIG);
		
		System.out.println("----");
		
		System.out.println("test 1 = " + aModel.contains(socratesIRI, null, null));
		System.out.println("test 2 = " + aModel.contains(aristotleIRI, null, null));
		System.out.println("test 3 = " + aModel.contains(socratesIRI, null, null, (Resource)null));
		System.out.println("test 4 = " + aModel.contains(aristotleIRI, null, null, (Resource)null));
		System.out.println("test 5 = " + aModel.contains(socratesIRI, null, null, ctx));
		System.out.println("test 6 = " + aModel.contains(aristotleIRI, null, null, ctx));

		System.out.println("----");
		
		Model nameModel = aModel.filter(socratesIRI, FOAF.NAME, null);
		
		Rio.write(nameModel, System.out, RDFFormat.TRIG);
		
		System.out.println("----");

		System.out.println("out 1 = " + nameModel.subjects());
		System.out.println("out 2 = " + nameModel.objects());
		System.out.println("out 3 = " + Models.objectStrings(nameModel));
		System.out.println("out 4 = " + Models.objectLiterals(nameModel));
		
		nameModel.add(socratesIRI, FOAF.NAME, fact.createLiteral("Σωκράτης", "grc")); // ancient greek!
		
		System.out.println("----");

		Rio.write(aModel, System.out, RDFFormat.TRIG);

	}
	
}
