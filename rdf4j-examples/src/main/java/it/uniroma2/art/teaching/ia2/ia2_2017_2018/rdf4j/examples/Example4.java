package it.uniroma2.art.teaching.ia2.ia2_2017_2018.rdf4j.examples;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFParseException;

public class Example4 {
	public static void main(String[] args) throws RDFParseException, RepositoryException, IOException {
		Repository rep = new SPARQLRepository("http://dbpedia.org/sparql");
		rep.initialize();
		try {
			try (RepositoryConnection conn = rep.getConnection()) {
				ValueFactory fact = conn.getValueFactory();

				TupleQuery birthPlaceQuery = conn.prepareTupleQuery(
						// @formatter:off
						"prefix dbo: <http://dbpedia.org/ontology/>\n" +
						"select ?place where {\n" +
						"    ?subject dbo:birthPlace ?place . \n" +
						"}"
						// @formatter:on
				);

				birthPlaceQuery.setBinding("subject", fact.createIRI("http://dbpedia.org/resource/Socrates"));
				Set<BindingSet> socratesBirthPlaces = QueryResults.asSet(birthPlaceQuery.evaluate());
				System.out.println("socrates's birthplaces according to DBpedia: " + socratesBirthPlaces);

				Set<Value> socratesBirthPlaces2 = QueryResults.stream(birthPlaceQuery.evaluate()).map(bs -> bs.getValue("place"))
						.collect(Collectors.toSet());

				System.out.println(
						"socrates's birthplaces according to DBpedia (second version): " + socratesBirthPlaces2);
			}
		} finally {
			rep.shutDown();
		}
	}
}
